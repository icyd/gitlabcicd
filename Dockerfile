FROM node:10.12-slim

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
WORKDIR /usr/src/app/
COPY package.json ./
RUN npm install -g yarn
RUN yarn install
COPY . ./
RUN yarn compile

EXPOSE 8080

CMD ["node", "app/index.js"]
