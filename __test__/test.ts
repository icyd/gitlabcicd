import request from 'request';

const base_url = 'http://localhost:8080/';

describe('Hello welcomepage', () => {
    describe('GET /', () => {
        it('Returns 200 code', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
            });
        });
    });
});
