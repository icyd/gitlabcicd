import express from 'express';

const app = express();

app.get('/', (req: express.Request, res: express.Response) => {
  res.status(200).send('Hello From GitLab CI/CD');
});

app.listen(8080);
